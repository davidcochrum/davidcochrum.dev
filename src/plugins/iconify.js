import AOS from 'aos'
import 'aos/dist/aos.css'

export default {
  install: (app) => {
    app.AOS = new AOS.init({ disable: 'phone' })
  },
}
