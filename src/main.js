import { createApp } from 'vue'
import AOS from '@/plugins/aos'
import App from '@/App.vue'
import CoreUI from '@/plugins/coreui'
import Iconify from '@/plugins/iconify'
import Lottie from '@/plugins/lottie'

createApp(App).use(AOS).use(CoreUI).use(Iconify).use(Lottie).mount('#app')
