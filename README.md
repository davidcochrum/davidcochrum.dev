# David Cochrum's Portfolio
[![pipeline status](https://gitlab.com/davidcochrum/chrum.me/badges/main/pipeline.svg)](https://gitlab.com/davidcochrum/chrum.me/-/commits/main)

So you dig a bit deeper than just visiting the website, eh? Kudos to you. Well here's the source code which is compiled
to serve [Chrum.me](https://chrum/me) statically on [GitLab Pages](https://gitlab.com/davidcochrum/chrum.me).

I borrowed a [starter template from Hanzla Tauqeer](https://github.com/1hanzla100/developer-portfolio).
However, I prefer the clean component files in Vue much more than React, so I wanted to convert it.
Switching from Next to Nuxt was an option, but both felt like overkill for this single landing page so this is composed of just basic Vue components.

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
